/*
SQLyog Community v12.18 (64 bit)
MySQL - 5.5.47-0ubuntu0.14.04.1 : Database - ihsan_new
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ihsan_new` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ihsan_new`;

/*Table structure for table `agenda` */

DROP TABLE IF EXISTS `agenda`;

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_agenda` varchar(255) DEFAULT NULL,
  `ruangan` int(11) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `status` enum('aktif','tidak aktif') DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`),
  KEY `ruangan` (`ruangan`),
  CONSTRAINT `agenda_ibfk_1` FOREIGN KEY (`ruangan`) REFERENCES `ruangan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `agenda` */

/*Table structure for table `agenda_peserta` */

DROP TABLE IF EXISTS `agenda_peserta`;

CREATE TABLE `agenda_peserta` (
  `id_agenda` int(11) NOT NULL,
  `id_peserta` int(11) NOT NULL,
  `status_kehadiran` enum('hadir','tidak hadir') DEFAULT NULL,
  PRIMARY KEY (`id_agenda`,`id_peserta`),
  KEY `id_peserta` (`id_peserta`),
  CONSTRAINT `agenda_peserta_ibfk_1` FOREIGN KEY (`id_agenda`) REFERENCES `agenda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `agenda_peserta_ibfk_2` FOREIGN KEY (`id_peserta`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `agenda_peserta` */

/*Table structure for table `forum` */

DROP TABLE IF EXISTS `forum`;

CREATE TABLE `forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `isi` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `forum_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `forum` */

insert  into `forum`(`id`,`judul`,`isi`,`created_at`,`created_by`) values 
(4,'Ini perjalanan ke jogjanya jadi gak?','Sebenarnya ini perjalanan kejogjany jadi apa enggak sih? banyak tim yang kebingungan,,,','2016-02-07 13:33:36',3);

/*Table structure for table `forum_komentar` */

DROP TABLE IF EXISTS `forum_komentar`;

CREATE TABLE `forum_komentar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_forum` int(11) DEFAULT NULL,
  `isi` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_forum` (`id_forum`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `forum_komentar_ibfk_1` FOREIGN KEY (`id_forum`) REFERENCES `forum` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `forum_komentar_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `forum_komentar` */

/*Table structure for table `profesi` */

DROP TABLE IF EXISTS `profesi`;

CREATE TABLE `profesi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_profesi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `profesi` */

insert  into `profesi`(`id`,`nama_profesi`,`keterangan`) values 
(1,'Kameramena','ini profesi untuk kameramena'),
(5,'Editor','Profesi yang mengurusi mengenai semua hal yang berhubungan dengan editing sebuah projek, baik itu desain, maupun edit video.');

/*Table structure for table `ruangan` */

DROP TABLE IF EXISTS `ruangan`;

CREATE TABLE `ruangan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruangan` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `status` enum('aktif','tidak aktif') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ruangan` */

insert  into `ruangan`(`id`,`nama_ruangan`,`keterangan`,`status`) values 
(3,'Ruang rapat utama','Disini tempat ruang rapat utama kita','aktif');

/*Table structure for table `surat` */

DROP TABLE IF EXISTS `surat`;

CREATE TABLE `surat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tujuan_dinas` varchar(255) DEFAULT NULL,
  `biaya_transportasi` varchar(255) DEFAULT NULL,
  `biaya_penginapan` varchar(255) DEFAULT NULL,
  `biaya_konsumsi` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `status` enum('disetujui','menunggu','ditolak') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `surat` */

insert  into `surat`(`id`,`tujuan_dinas`,`biaya_transportasi`,`biaya_penginapan`,`biaya_konsumsi`,`tanggal`,`keterangan`,`status`) values 
(1,'Bandung lautan api','1200000','1000000','1000000','2016-02-29','Ini merupakan perjalanan dinas untuk mempermudah proses produksi dengan melakukan pengambilan semua aset produksi dilapangan','disetujui');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` enum('staff','manager') DEFAULT NULL,
  `alamat` text,
  `email` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`nama`,`jabatan`,`alamat`,`email`,`no_hp`,`last_login`) values 
(1,'andriyas','andri123','andriyas efendi','manager','puluhdadi','admin@admin.com','0852754643786','2016-02-07 14:14:47'),
(3,'efendia','efendia','andri kameramen','staff','efendi','efendi@mail.com','085672314618','2016-02-07 14:13:21'),
(5,'efendi','efendi','andri presenter','staff','efendi','efendi@mail.com','085672314618','0000-00-00 00:00:00'),
(6,'fendi','fendi','andri editor','staff','fendi','fendi@mail.com','085672314618','0000-00-00 00:00:00'),
(7,'dia','aku','dia kameramen','staff','puluhdadi','dia@gmail.com','085627131233','0000-00-00 00:00:00'),
(8,'lisa','lisaku','lisa','staff','lisa','lisa@gmail.com','085672314618','0000-00-00 00:00:00'),
(9,'ihsan','ihsan','Ihsan nur akbar','staff','jogjakarta','ihsan@gmail.dev','081234535234','0000-00-00 00:00:00');

/*Table structure for table `user_profesi` */

DROP TABLE IF EXISTS `user_profesi`;

CREATE TABLE `user_profesi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profesi` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_profesi` (`id_profesi`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `user_profesi_ibfk_1` FOREIGN KEY (`id_profesi`) REFERENCES `profesi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_profesi_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `user_profesi` */

insert  into `user_profesi`(`id`,`id_profesi`,`id_user`) values 
(1,1,3),
(4,1,7),
(6,1,9);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
