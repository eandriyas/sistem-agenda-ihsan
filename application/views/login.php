<?php $this->load->view('_header'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-2">

            </div>
            <div class="col-md-8">
                <div class="panel panel-default form-login">
                    <div class="panel-heading">
                        Login

                    </div>
                    <div class="panel-body">
                        <?php if ($this->session->flashdata('error')): ?>
                            <div class="alert alert-danger"
                                 role="alert"><?php echo $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <form action="<?php echo base_url('auth/login_proses'); ?>" method="post" role="form">


                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" class="form-control" name="username" id=""
                                       placeholder="Input your username">
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password"
                                       placeholder="Input your password">
                            </div>
                            <a href="<?php echo base_url('auth/daftar'); ?>">Register</a><br><br>


                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2">

            </div>
        </div>

    </div>
<?php $this->load->view('_footer'); ?>