<?php $this->load->view('_header'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-2">

            </div>
            <div class="col-md-8">
                <div class="panel panel-default form-login">
                    <div class="panel-heading">
                        Form Pendaftaran

                    </div>
                    <div class="panel-body">
                        <?php if ($this->session->flashdata('error')): ?>
                            <div class="alert alert-danger"
                                 role="alert"><?php echo $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <form action="<?php echo current_url(); ?>" method="post" role="form">


                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" class="form-control" name="username" id=""
                                       placeholder="Input your username">
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password"
                                       placeholder="Input your password">
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama anda">
                            </div>
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <textarea name="alamat" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Masukkan email anda">
                            </div>
                            <div class="form-group">
                                <label for="">No Hp</label>
                                <input type="text" name="hp" class="form-control" placeholder="Masukkan no hp anda">
                            </div>
                            <div class="form-group">
                                <label for="">Profesi</label>
                                <select name="profesi" class="form-control" id="">
                                    <?php foreach ($profesi as $p): ?>
                                        <option value="<?php echo $p->id; ?>"><?php echo ucwords($p->nama_profesi); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>



                            <button type="submit" class="btn btn-primary">Daftar</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2">

            </div>
        </div>

    </div>
<?php $this->load->view('_footer'); ?>