<?php $this->load->view('manager/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php endif; ?>
            <div class="panel panel-default">
                <div class="panel-heading">Your profile</div>
                <div class="panel-body">
                    <form action="<?php echo current_url(); ?>" method="post" role="form">

                    	<div class="form-group">
                    		<label for="">Username</label>
                    		<input type="text" class="form-control" name="username" readonly id="" value="<?php echo $user->username; ?>">
                    	</div>
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control" name="nama" value="<?php echo $user->nama; ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email" value="<?php echo $user->email; ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" name="password" value="<?php echo $user->password; ?>">
                        </div>
                        <div class="form-group">
                            <label for="">No. Handphone</label>
                            <input type="text" class="form-control" name="hp" value="<?php echo $user->no_hp; ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <textarea style="min-height: 100px;;" name="alamat" id="" class="form-control"><?php echo $user->alamat; ?></textarea>

                        </div>



                    	<button type="submit" class="btn btn-primary">Rubah</button>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
<?php $this->load->view('manager/_footer'); ?>
