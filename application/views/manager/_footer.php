<footer>
</footer>
</div>
</div>
<div class="col-md-2">
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>/assets/datepicker/js/datepicker.js"></script>
<script src="<?php echo base_url(); ?>/assets/datepicker/js/i18n/datepicker.en.js"></script>
<script src="<?php echo base_url(); ?>/assets/sweet/sweetalert.min.js"></script>
<script>
	$('#datePicker').datepicker(
		);
</script>
<script>
$(document).ready(function(){
	$('.delete').on('click', function(){
		var href = $(this).attr('href');
		swal({   
			title: "Apakah anda yakin?",   
			text: "Kamu tidak akan bisa mengembalikan data ini lagi",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Ya, hapus saja!",   
			closeOnConfirm: false 
		}, 
		function(){   
			swal("Berhasil menghapus!", "Data anda berhasil dihapus.", "success"); 
			window.location.replace(href);
		});
		return false;
	});
	$('.ubah-komentar').on('click', function(){
		var form = $(this).attr('data-form');
		$('#'+form).toggle();
		return false;
	});
	$('.hapus-komentar').on('click', function(){
		var href = $(this).attr('href');
		swal({   
			title: "Apakah anda yakin?",   
			text: "Kamu tidak akan bisa mengembalikan data ini lagi",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Ya, hapus saja!",   
			closeOnConfirm: false 
		}, 
		function(){
			$.ajax({
				url : href, 
				success: function(){
					swal({   
						title: "Pesan",  
						text: "Berhasil menghapus komentar.",   
					}, 
					function(){  
						window.location.reload();
					});
				}
			});   
		});
		return false;
	});
	$('.btn-ubah').on('click', function(){
		var isi = $(this).attr('data-isi');
		var id = $(this).attr('data-id');
		var url_ajax = $(this).attr('data-url');
		var baru = $('#'+isi).val();
		if(baru == ''){
			swal({
				title: 'Warning!',
				text: "Isi komentar tidak boleh kosong!", 
				type: 'warning'
			});
		} else {
			$.ajax({
				url :url_ajax,
				type: 'post',
				dataType: 'json',
				data : {
					id_komentar : id,
					isi : baru
				},
				success : function(msg){
					if(msg.message == 'berhasil'){
						swal({   
							title: "Pesan",  
							text: "Berhasil merubah komentar.",   
						}, 
						function(){  
							window.location.reload();
						});	
					}
				}
			});
		}
	});
});
</script>
</body>
</html>
