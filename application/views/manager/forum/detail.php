<?php $this->load->view('manager/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
           <?php if ($this->session->flashdata('success')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php endif; ?>
        <h2><?php echo ucwords($forum->judul); ?></h2>
        <hr>
        <p class="lead"><?php echo $forum->isi; ?></p>
        <div class="row">
            <div class="col-md-6">
                <form action="<?php echo base_url('manager/forum/tambah_komentar') ?>" method="post" role="form">
                   <div class="form-group">
                      <label for="">Tambah komentar</label>
                      <textarea name="isi"  class="form-control" style="height: 100px;"></textarea>
                  </div>
                  <input type="hidden" name="id_forum" value="<?php echo $forum->id; ?>">

                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
      <hr>
      <ul class="list-group">
        <?php foreach ($komentar as $k) : ?>
            <li class="list-group-item">
                <strong><a href=""><?php echo ucwords($k->nama); ?></a></strong><br>
                <p style="margin-left: 20px;text-align: left;">
                    <small>
                    <?php echo $k->isi; ?> <a data-form="<?php echo $k->komentar_id; ?>" href="" class="ubah-komentar">Ubah</a> <a href="<?php echo base_url('manager/forum/hapusKomentar/'.$k->komentar_id) ?>" class="hapus-komentar">Hapus</a>
                    <span style="display:none"  id="<?php echo $k->komentar_id; ?>" class="ubah-form">
                        <textarea id="isi-<?php echo $k->komentar_id; ?>" class="form-control"><?php echo $k->isi; ?> </textarea><br>
                        <button data-url="<?php echo base_url('manager/forum/ubahKomentar') ?>" data-id="<?php echo $k->komentar_id; ?>" data-isi ="isi-<?php echo $k->komentar_id; ?>" class="btn btn-default btn-ubah">Ubah</button>
                    </span>
                    </small>
                </p>
            </li>
        <?php endforeach; ?>
    </ul>


</div>
</div>
</div>
<?php $this->load->view('manager/_footer'); ?>
