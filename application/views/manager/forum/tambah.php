<?php $this->load->view('manager/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php endif; ?>
            <h2>Tambah diskusi</h2>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <form action="<?php echo current_url(); ?>" method="post" role="form">
                        <div class="form-group">
                          <label for="judul">Judul</label>
                          <input id="judul" type="text" name="judul" class="form-control" value="">

                      </div>
                      <div class="form-group">
                          <label for="isi">Isi</label>
                          <textarea id="isi" name="isi"  class="form-control" style="height: 100px;"></textarea>
                      </div>

                      <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
              </div>
          </div>
          <hr>
     


    </div>
</div>
</div>
<?php $this->load->view('manager/_footer'); ?>
