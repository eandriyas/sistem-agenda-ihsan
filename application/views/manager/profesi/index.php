<?php $this->load->view('manager/_header'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <br>
                <a href="<?php echo base_url('manager/profesi/tambah'); ?>" class="btn btn-primary">Tambah Profesi</a>
                <br>
                <hr>
                <table class="table table bordered">
                    <tr>
                        <th>Nama profesi</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    <?php foreach($profesi as $p): ?>

                        <tr>
                           <td><?php echo $p->nama_profesi; ?></td>
                            <td><?php echo $p->keterangan; ?></td>
                            <td>
                                <a href="<?php echo base_url('manager/profesi/ubah/'.$p->id); ?>" class="btn btn-default">Ubah</a>
                                <a href="<?php echo base_url('manager/profesi/hapus/'.$p->id); ?>" class="btn btn-danger delete">Hapus</a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
<?php $this->load->view('manager/_footer'); ?>
