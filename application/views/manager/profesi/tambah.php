<?php $this->load->view('manager/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php endif; ?>
            <form action="<?php echo current_url(); ?>" method="post" role="form">
                <legend>Tambah Profesi</legend>

                <div class="form-group">
                    <label for="">Nama Profesi</label>
                    <input type="text" class="form-control" name="nama_profesi" id="" placeholder="Masukkan nama profesi">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" class="form-control" style="min-height: 100px;"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Tambah">
                    <input type="reset" class="btn btn-danger" value="Reset">
                </div>

            </form>
        </div>
    </div>
</div>

<?php $this->load->view('manager/_footer'); ?>
