<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/datepicker/css/datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/sweet/sweetalert.css">

</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            <div class="wrapper">
                <header>
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="<?php echo base_url('manager/home'); ?>">AFIS</a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="<?php echo empty($home_active) ? '': $home_active ; ?>"><a href="<?php echo base_url('manager/home'); ?>">Home <span class="sr-only">(current)</span></a></li>
                                    <li class="<?php echo empty($ruangan_active) ? '': $ruangan_active ; ?>"><a href="<?php echo base_url('manager/ruangan/index'); ?>">Ruangan </a></li>
                                    <li class="<?php echo empty($profesi_active) ? '': $profesi_active ; ?>"><a href="<?php echo base_url('manager/profesi/index'); ?>">Profesi </a></li>
                                    <li class="<?php echo empty($rapat_active) ? '': $rapat_active ; ?>"><a href="<?php echo base_url('manager/agenda/index'); ?>">Agenda</a></li>
                                    <li class="<?php echo empty($surat_active) ? '': $surat_active ; ?>"><a href="<?php echo base_url('manager/surat/index'); ?>">Surat Kerja</a></li>
                                    <li class="<?php echo empty($forum_active) ? '': $forum_active ; ?>"><a href="<?php echo base_url('manager/forum/index'); ?>">Forum <span class="badge"></span></a></li>

                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true" aria-expanded="false"><?php $user = $this->session->userdata('manager'); echo $user['username']; ?> <span
                                                class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url('manager/profile/edit/'.$user['user_id']); ?>">Profile</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="<?php echo base_url('auth/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </header>