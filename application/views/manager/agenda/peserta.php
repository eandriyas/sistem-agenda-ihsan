<?php $this->load->view('manager/_header'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <br>
                <!-- <a href="<?php echo base_url('manager/agenda/tambah'); ?>" class="btn btn-primary">Tambah Agenda</a> -->
                <br>
                <hr>
                <table class="table table bordered">
                    <tr>
                        <th>Nama peserta</th>
                        <th>Profesi</th>
                        <th>Status kehadiran</th>
                        <!-- <th>Aksi</th> -->
                    </tr>
                    <?php foreach($peserta as $p): ?>
                        <tr>
                            <td><?php echo ucwords($p->nama); ?></td>
                            <td><?php echo ucwords($p->nama_profesi); ?></td>
                            <td><?php echo ucwords($p->status_kehadiran); ?></td>
                           <!--  <td>
                                <a href="<?php echo base_url('manager/agenda/peserta/'.$p->user_id); ?>" class="btn btn-primary">Peserta</a>
                                <a href="<?php echo base_url('manager/agenda/ubah/'.$p->user_id); ?>" class="btn btn-default">Ubah</a>
                                <a href="<?php echo base_url('manager/agenda/hapus/'.$p->user_id); ?>" class="btn btn-danger">Hapus</a>
                            </td> -->
                        </tr>

                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
<?php $this->load->view('manager/_footer'); ?>
