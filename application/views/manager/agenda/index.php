<?php $this->load->view('manager/_header'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <br>
                <a href="<?php echo base_url('manager/agenda/tambah'); ?>" class="btn btn-primary">Tambah Agenda</a>
                <br>
                <hr>
                <table class="table table bordered">
                    <tr>
                        <th>Nama agenda</th>
                        <th>Ruangan</th>
                        <th>Waktu Rapat</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                    <?php foreach($agenda as $a): ?>
                        <tr>
                            <td><?php echo ucwords($a->nama_agenda); ?></td>
                            <td><?php echo ucwords($a->nama_ruangan); ?></td>
                            <td><?php echo date('F d, Y H:i:s', strtotime($a->waktu)); ?></td>
                            <td><?php echo ucwords($a->status);?></td>
                            <td>
                                <a href="<?php echo base_url('manager/agenda/peserta/'.$a->id); ?>" class="btn btn-primary">Peserta</a>
                                <a href="<?php echo base_url('manager/agenda/ubah/'.$a->id); ?>" class="btn btn-default">Ubah</a>
                                <a href="<?php echo base_url('manager/agenda/hapus/'.$a->id); ?>" class="btn btn-danger delete">Hapus</a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
<?php $this->load->view('manager/_footer'); ?>
