<?php $this->load->view('manager/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">

            <table class="table table bordered">
                <tr>
                    <th>Tujuan</th>
                    <th>Biaya Transportasi</th>
                    <th>Biaya Penginapan</th>
                    <th>Biaya Konsumsi</th>
                    <th>Waktu</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
                <?php foreach($surat as $s): ?>
                    <tr>
                        <td><?php echo $s->tujuan_dinas; ?></td>
                        <td>Rp. <?php echo $s->biaya_transportasi; ?></td>
                        <td>Rp. <?php echo $s->biaya_penginapan; ?></td>
                        <td>Rp. <?php echo $s->biaya_konsumsi; ?></td>
                        <td><?php echo date('F l, Y H:i:s', strtotime($s->tanggal)); ?></td>
                        <td><?php echo $s->status; ?></td>
                        <td>
                            <a href="<?php  echo base_url('manager/surat/ubah/'.$s->id); ?>" class="btn btn-default">Ubah</a>
                            <a href="<?php echo base_url('manager/surat/hapus/'.$s->id); ?>" onclick="return false;" class="btn btn-danger delete">Hapus</a>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('manager/_footer'); ?>
