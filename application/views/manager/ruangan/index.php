<?php $this->load->view('manager/_header'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if($this->session->flashdata('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <br>
                <a href="<?php echo base_url('manager/ruangan/tambah'); ?>" class="btn btn-primary">Tambah Ruangan</a>
                <br>
                <hr>
                <table class="table table bordered">
                    <tr>
                        <th style="width: 200px;">Nama Ruangan</th>
                        <th>Status</th>
                        <th style="width: 300px;">Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    <?php foreach($ruangan as $r): ?>
                        <tr>
                            <td><?php echo ucwords($r->nama_ruangan); ?></td>
                            <td><?php echo ucwords($r->status); ?></td>
                            <td><?php echo $r->keterangan; ?></td>
                            <td>
                                <a href="<?php echo base_url('manager/ruangan/ubah/'.$r->id); ?>" class="btn btn-default">Ubah</a>
                                <a href="<?php echo base_url('manager/ruangan/hapus/'.$r->id); ?>" class="btn btn-danger delete">Hapus</a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
<?php $this->load->view('manager/_footer'); ?>
