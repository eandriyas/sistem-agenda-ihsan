<?php $this->load->view('staff/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <br>
            <a href="<?php echo base_url('staff/forum/tambah'); ?>" class="btn btn-primary">Tambah diskusi</a>
            <br>
            <hr>
            <table class="table table bordered">
                <tr>
                    <th>Judul</th>
                    <th>Dibuat pada</th>
                    <th>Aksi</th>
                </tr>
                <?php foreach ($forum as $f) : ?>
                    <tr>
                        <td><?php echo ucwords($f->judul) ;?></td>
                        <td><?php echo date('F l, Y H:i:s', strtotime($f->created_at)); ?></td>
                        <td>
                            <a href="<?php echo base_url('staff/forum/detail/'.$f->id); ?>" class="btn btn-info">Detail</a>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('staff/_footer'); ?>
