<?php $this->load->view('staff/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
           <?php if ($this->session->flashdata('success')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php endif; ?>
        <h2><?php echo ucwords($forum->judul); ?></h2>
        <hr>
        <p class="lead"><?php echo $forum->isi; ?></p>
        <div class="row">
            <div class="col-md-6">
                <form action="<?php echo base_url('staff/forum/tambah_komentar') ?>" method="post" role="form">
                   <div class="form-group">
                      <label for="">Tambah komentar</label>
                      <textarea name="isi"  class="form-control" style="height: 100px;"></textarea>
                  </div>
                  <input type="hidden" name="id_forum" value="<?php echo $forum->id; ?>">

                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
      <hr>
      <ul class="list-group">
        <?php foreach ($komentar as $k) : ?>
            <li class="list-group-item">
                <strong><a href=""><?php echo ucwords($k->nama); ?></a></strong><br>
                <p style="margin-left: 20px;text-align: left;">
                    <small>
                    <?php echo $k->isi; ?>
                    </small>
                </p>
            </li>
        <?php endforeach; ?>
    </ul>


</div>
</div>
</div>
<?php $this->load->view('staff/_footer'); ?>
