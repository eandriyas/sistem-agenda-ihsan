<?php $this->load->view('staff/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
        <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php endif; ?>
        <a href="<?php echo base_url('staff/surat/tambah') ?>" class="btn btn-primary">Tambah Surat</a>
<br>
            <table class="table table bordered">
                <tr>
                    <th>Tujuan</th>
                    <th>Biaya Transportasi</th>
                    <th>Biaya Penginapan</th>
                    <th>Biaya Konsumsi</th>
                    <th>Waktu</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
                <?php foreach($surat as $s): ?>
                    <tr>
                        <td><?php  echo $s->tujuan_dinas; ?></td>
                        <td><?php echo $s->biaya_transportasi; ?></td>
                        <td>Rp. <?php echo $s->biaya_penginapan; ?></td>
                        <td>Rp. <?php echo $s->biaya_konsumsi; ?></td>
                        <td><?php echo date('F l, Y H:i:s;', strtotime($s->tanggal)); ?></td>
                        <td><?php echo $s->status; ?></td>
                        <td>
                            <a href="<?php  echo base_url('staff/surat/ubah/'.$s->id); ?>" class="btn btn-default">Ubah</a>
                            <a href="<?php echo base_url('staff/surat/hapus/'.$s->id); ?>" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('staff/_footer'); ?>
