<?php $this->load->view('staff/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php endif; ?>
            <form action="<?php echo current_url(); ?>" method="post" role="form">
                <legend>Ubah Surat</legend>

                <div class="form-group">
                    <label for="">Tujuan dinas</label>
                    <input value="<?php echo $surat->tujuan_dinas; ?>" type="text" class="form-control" name="tujuan_dinas" id="" placeholder="Masukkan tujuan dinas">
                </div>
               <div class="form-group">
                   <label for="">Biaya transportasi</label>
                   <input value="<?php echo $surat->biaya_transportasi; ?>" type="text" name="biaya_transportasi" class="form-control" placeholder="Masukkan biaya transportasi">
               </div>
               <div class="form-group">
                   <label for="">Biaya penginapan</label>
                   <input value="<?php echo $surat->biaya_penginapan; ?>" type="text" name="biaya_penginapan" placeholder="Masukkan biaya penginapan" class="form-control">
               </div>
               <div class="form-group">
                   <label for="">Biaya konsumsi</label>
                   <input value="<?php echo $surat->biaya_konsumsi; ?>" type="text" name="biaya_konsumsi" placeholder="Masukkan biaya konsumsi" class="form-control">
               </div>
                <div class="form-group">
                    <label for="">Tanggal Surat</label>
                    <input id="datePicker"
                    value="<?php echo $surat->tanggal; ?>" 
                    data-position="top left"
                    data-language="en"
                    data-date-format="yyyy-mm-dd"
                    type="text" name="tanggal"
                    class="form-control datepicker-here" placeholder="Pilih waktu agenda">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" class="form-control" style="min-height: 100px;"><?php echo $surat->keterangan; ?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Ubah">
                    <input type="reset" class="btn btn-danger" value="Reset">
                </div>

            </form>
        </div>
    </div>
</div>

<?php $this->load->view('staff/_footer'); ?>
