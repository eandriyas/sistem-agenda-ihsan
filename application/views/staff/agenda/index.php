<?php $this->load->view('staff/_header'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <br>

                <br>
                <table class="table table bordered">
                    <tr>
                        <th>Nama agenda</th>
                        <th>Ruangan</th>
                        <th>Waktu Rapat</th>
                        <th>Status</th>
                        <th>Status kehadiran</th>
                        <th>Aksi</th>
                    </tr>
                    <?php foreach($agenda as $a): ?>
                        <tr>
                            <td><?php echo ucwords($a->nama_agenda); ?></td>
                            <td><?php echo ucwords($a->nama_ruangan); ?></td>
                            <td><?php echo date('F d, Y H:i:s', strtotime($a->waktu)); ?></td>
                            <td><?php echo ucwords($a->status);?></td>
                            <td><?php echo ucwords($a->status_kehadiran); ?></td>
                            <td>
                                <a href="<?php echo base_url('staff/agenda/ubah/'.$a->id_peserta.'/'.$a->id_agenda.'/hadir'); ?>" class="btn btn-default">Hadir</a>
                                <a href="<?php echo base_url('staff/agenda/ubah/'.$a->id_peserta.'/'.$a->id_agenda.'/tidak hadir'); ?>" class="btn btn-danger">Tidak hadir</a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
<?php $this->load->view('staff/_footer'); ?>
