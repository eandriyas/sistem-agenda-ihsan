<?php $this->load->view('manager/_header'); ?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php endif; ?>
            <form action="<?php echo current_url(); ?>" method="post" role="form">
                <legend>Ubah Agenda</legend>

                <div class="form-group">
                    <label for="">Nama Agenda</label>
                    <input type="text" class="form-control" name="nama_agenda" id=""
                           value="<?php echo $agenda->nama_agenda; ?>" placeholder="Masukkan nama agenda">
                </div>
                <div class="form-group">
                    <label for="">Ruangan</label>
                    <select name="ruangan" id="" class="form-control">
                        <option value="<?php echo $agenda->ruangan_id; ?>"><?php echo $agenda->nama_ruangan; ?></option>
                        <?php foreach ($ruangan as $r): ?>
                            <option value="<?php echo $r->id; ?>"><?php echo ucwords($r->nama_ruangan); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Tanggal agenda</label>
                    <input id="datePicker"
                           data-position="top left"
                           data-language="en"
                           data-date-format="yyyy-mm-dd"
                           type="text" name="tanggal"
                           value="<?php echo date('Y-m-d', strtotime($agenda->waktu)); ?>"
                           class="form-control datepicker-here" placeholder="Pilih waktu agenda">
                </div>
                <div class="form-group">
                    <label for="">Waktu agenda</label>
                    <div class="input-group">
                        <div class="input-group-addon">Jam</div>
                        <select name="jam" id="" class="form-control">
                            <option value="<?php echo date('H', strtotime($agenda->waktu)); ?>"><?php echo date('H', strtotime($agenda->waktu)); ?></option>
                            <?php for ($i = 0; $i < 24; $i++): ?>
                                <?php if ($i < 10): ?>
                                    <option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
                                <?php else : ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </select>
                        <div class="input-group-addon">Menit</div>
                        <select name="menit" id="" class="form-control">
                            <option value="<?php echo date('i', strtotime($agenda->waktu)); ?>"><?php echo date('i', strtotime($agenda->waktu)); ?></option>
                            <?php for ($i = 0; $i < 60; $i += 10): ?>
                                <?php if ($i < 10): ?>
                                    <option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
                                <?php else : ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <select name="status" class="form-control" id="">
                        <option value="<?php echo $agenda->status; ?>"><?php echo ucwords($agenda->status); ?></option>
                        <option value="aktif">Aktif</option>
                        <option value="tidak aktif">Tidak aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" class="form-control" style="min-height: 100px;"><?php echo $agenda->keterangan; ?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Ubah">
                    <input type="reset" class="btn btn-danger" value="Reset">
                </div>

            </form>
        </div>
    </div>
</div>
<?php $this->load->view('manager/_footer'); ?>
