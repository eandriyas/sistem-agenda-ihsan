<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_komentar extends CI_Model
{

    function semua_komentar($id_forum){
        $this->db->select('*, forum_komentar.id as komentar_id');
        $this->db->from('forum_komentar');
        $this->db->join('user', 'forum_komentar.created_by = user.id');
        $this->db->where('id_forum', $id_forum);
        $this->db->order_by('id_forum', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    function tambah($data){
        $query = $this->db->insert('forum_komentar', $data);

        return $query;
    }
    function ubah($id, $data){
        $this->db->where('id', $id);
        $query = $this->db->update('forum_komentar', $data);
        return $query;
    }
    function hapus($id){
        $this->db->from('forum_komentar');
        $this->db->where('id', $id);
        $query = $this->db->delete();
        return $query;
    }
}
