<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ruangan extends CI_Model
{


    public function tambah($data)
    {
        $query = $this->db->insert('ruangan', $data);
        return $query;

    }

    public function rubah($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('ruangan', $data);

        return $query;

    }

    public function hapus($id)
    {
        $this->db->from('ruangan');
        $this->db->where('id', $id);
        $query = $this->db->delete();

        return $query;
    }

    public function semua_ruangan()
    {
        $this->db->from('ruangan');
        $query = $this->db->get();

        return $query->result();


    }

    public function satu_ruangan($id)
    {
        $this->db->from('ruangan');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }


}
