<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_agenda extends CI_Model
{


    public function tambah($data)
    {
        $query = $this->db->insert('agenda', $data);
        return $query;

    }

    public function rubah($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('agenda', $data);

        return $query;

    }

    public function hapus($id)
    {
        $this->db->from('agenda');
        $this->db->where('id', $id);
        $query = $this->db->delete();

        return $query;
    }

    public function semua_agenda()
    {
        $this->db->select('agenda.*, ruangan.nama_ruangan');
        $this->db->from('agenda');
        $this->db->join('ruangan', 'ruangan.id = agenda.ruangan');
        $query = $this->db->get();

        return $query->result();


    }

    public function satu_agenda($id)
    {
        $this->db->select('agenda.*, ruangan.nama_ruangan, ruangan.id as ruangan_id');
        $this->db->from('agenda');
        $this->db->join('ruangan', 'ruangan.id = agenda.ruangan');
        $this->db->where('agenda.id', $id);
        $query = $this->db->get();

        return $query->row();
    }
    public function agenda_peserta($id){
        $this->db->select('*, user.id as peserta_id');
        $this->db->from('agenda_peserta');
        $this->db->join('user', 'agenda_peserta.id_peserta = user.id');
        $this->db->where('agenda_peserta.id_agenda', $id);
        $query = $this->db->get();

        return $query->result();
    }

    public function tambah_peserta($data){
        $query = $this->db->insert('agenda_peserta', $data);
        return $query;
    }
    public function peserta($id_agenda){
        $this->db->select('agenda_peserta.*, user.*, user.id as user_id, profesi.*');
        $this->db->from('agenda_peserta');
        $this->db->join('user', 'agenda_peserta.id_peserta = user.id');
        $this->db->join('user_profesi', 'agenda_peserta.id_peserta = user_profesi.id_user');
        $this->db->join('profesi', 'profesi.id = user_profesi.id_profesi');
        $this->db->where('agenda_peserta.id_agenda', $id_agenda);

        $query = $this->db->get();

        return $query->result();

    }

     public function semua_agenda_peserta($id_peserta)
    {
        $this->db->select('agenda.*, ruangan.nama_ruangan, agenda_peserta.*');
        $this->db->from('agenda');
        $this->db->join('ruangan', 'ruangan.id = agenda.ruangan');
        $this->db->join('agenda_peserta', 'agenda_peserta.id_agenda = agenda.id');
        $this->db->where('agenda_peserta.id_peserta', $id_peserta);
        $query = $this->db->get();

        return $query->result();


    }
    public function ubah_peserta($id_peserta, $id_agenda, $status){
        $this->db->where('id_agenda', $id_agenda);
        $this->db->where('id_peserta', $id_peserta);
        $query = $this->db->update('agenda_peserta', $status);
        return $query;
    }

    public function count_status_kehadiran($id_peserta){
        $this->db->from('agenda_peserta');
        $this->db->where('id_peserta', $id_peserta);
        $this->db->where('status_kehadiran is null');
         $query = $this->db->count_all_results();

         return $query;
    }



}
