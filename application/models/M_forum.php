<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_forum extends CI_Model
{

function semua_forum(){
    $this->db->from('forum');
    $this->db->order_by('created_at', 'desc');
    $query = $this->db->get();

    return $query->result();
}

function tambah($data){
    $query = $this->db->insert('forum', $data);

    return $query;
}
function satu_forum($id){
    $this->db->from('forum');
    $this->db->where('id', $id);
    $query = $this->db->get();

    return $query->row();
}
function ubah($id, $data){
    $this->db->where('id', $id);
    $query = $this->db->update('forum', $data);
    return $query;
}
function hapus($id){
    $this->db->from('forum');
    $this->db->where('id', $id);
    $query = $this->db->delete();

    return $query;
}

}
