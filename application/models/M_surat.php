<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_surat extends CI_Model
{

    public function semua_surat(){
        $this->db->from('surat');
        $query = $this->db->get();

        return $query->result();
    }
    public function satu_surat($id_surat){
        $this->db->from('surat');
        $this->db->where('id', $id_surat);
        $query = $this->db->get();
        return $query->row();
    }

    public function tambah($data)
    {
        $query = $this->db->insert('surat', $data);
        return $query;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('surat', $data);

        return $query;

    }
    public function hapus($id){
        $this->db->from('surat');
        $this->db->where('id', $id);
        $query = $this->db->delete();
        return $query;
    }


}
