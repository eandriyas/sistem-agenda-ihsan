<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_profesi extends CI_Model
{


    public function tambah($data)
    {
        $query = $this->db->insert('profesi', $data);
        return $query;

    }
    public function user_tambah($data)
    {
        $query = $this->db->insert('user_profesi', $data);
        return $query;

    }

    public function rubah($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('profesi', $data);

        return $query;

    }

    public function hapus($id)
    {
        $this->db->from('profesi');
        $this->db->where('id', $id);
        $query = $this->db->delete();

        return $query;
    }

    public function semua_profesi()
    {

        $this->db->from('profesi');
        $query = $this->db->get();

        return $query->result();


    }

    public function satu_profesi($id)
    {

        $this->db->from('profesi');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }
    public function user_profesi(){
        $this->db->select('user_profesi.*, user_profesi.id_profesi as profesi_id, profesi.*');
        $this->db->from('user_profesi');
        $this->db->join('profesi', 'user_profesi.id_profesi = profesi.id');
        // $this->db->join('user', 'user.id = user_profesi.id_user');
        $this->db->group_by('profesi.id');
        $query = $this->db->get();

        return $query->result();
    }
    public function staff_profesi($id_profesi){
        $this->db->select('user_profesi.*, user.*, user.id as user_id');
        $this->db->from('user_profesi');
        $this->db->join('user', 'user.id = user_profesi.id_user');
        $this->db->where('user_profesi.id_profesi', $id_profesi);
        $query = $this->db->get();

        return $query->result();
    }


}
