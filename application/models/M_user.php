<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model
{


    public function cek_login($username, $password)
    {
        $this->db->from('user');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get();

        return $query->row();
    }
    public function get_user($id){
        $this->db->from('user');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function tambah($data)
    {
        $query = $this->db->insert('user', $data);
        return $query;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('user', $data);

        return $query;

    }


}
