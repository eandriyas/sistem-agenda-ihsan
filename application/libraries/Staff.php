<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* class untuk manajemen staff
*/
class Staff 
{
	 public function __construct()
    {
        $this->CI =& get_instance();
    }
	
	public function get_staff($id_profesi){

		$this->CI->load->model('m_profesi');
		$main = $this->CI->m_profesi->staff_profesi($id_profesi);
		return $main;
	}
	
}