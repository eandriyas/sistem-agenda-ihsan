<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

// class untuk mengatur agenda oleh admin
class Agenda extends Auth
{

    public function __construct()
    {
        parent::__construct();
        // cek apakah user yang login manager
        $this->is_login_manager();
        // meload library staff
        $this->load->library('staff');


    }
    // halaman index untuk agenda yang menampilkan list/daftar dari agenda yang ada
    public function index()
    {
        // beri variable active pada menu bar
        $data['rapat_active'] = 'active';
        // title dari halaman
        $data['title'] = 'Halaman Rapat';
        // menampilkan semua daftar agenda dari databaase
        $data['agenda'] = $this->m_agenda->semua_agenda();
        // menampilkan view dari halaman index dan memasukkan data agenda yang ada
        $this->load->view('manager/agenda/index', $data);
    }
// fungsi untuk menambah agenda baru
    public function tambah()
    {
        // beri variable active pada menu bar
        $data['rapat_active'] = 'active';
        // title dari halaman
        $data['title'] = 'Halaman tambah rapat';
        // menampilkan semua daftar ruangan yang ada
        $data['ruangan'] = $this->m_ruangan->semua_ruangan();
        // menampilkan semua profesi yang ada
        $data['profesi'] = $this->m_profesi->user_profesi();
        // validasi input dari form tambah
        $this->form_validation->set_rules('nama_agenda', 'Nama Agenda', 'required');
        $this->form_validation->set_rules('ruangan', 'Ruangan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jam', 'Jam', 'required');
        $this->form_validation->set_rules('menit', 'Menit', 'required');
// jika input post tidak kosong
        if(!empty($_POST)){
            // memasukkan data input karyawan ke dalam variable
            $staff = $this->input->post('staff_profesi');
            // waktu dari agenda
            $waktu = $this->input->post('tanggal').' '.$this->input->post('jam').':'.$this->input->post('menit').':00';
            // jika validasi berhasil maka fungsi dijalankan
            if($this->form_validation->run() === true){
                // memasukkan data input kedalam sebuah array
                $data = [
                    'nama_agenda' => $this->input->post('nama_agenda'),
                    'ruangan' => $this->input->post('ruangan'),
                    'waktu' => date('Y-m-d H:i:s', strtotime($waktu)),
                    'status' => 'aktif',
                    'keterangan' => $this->input->post('keterangan')
                ];
                // tambah data kedalam database
                $tambah = $this->m_agenda->tambah($data);
                // mengambil data id yang baru saja di tambahkan kedalam database
                $id_agenda = $this->db->insert_id();
                // menghitung jumlah atau panjang dari array
                $a_staff = count($staff)-1;
                // melakukan input kedalam database untuk agenda peserta
                for ($i=0; $i<=$a_staff; $i++) {
                    // data yang akan dimasukkan kedalam database
                    $data = [
                        'id_agenda' => $id_agenda,
                        'id_peserta' => $staff[$i]
                    ];
                    // fungsi untuk menambah data
                    $this->m_agenda->tambah_peserta($data);
                }
                // struktur kendali jika penambahan kedatabase berhasil
                if($tambah){
                    // mengatur pesan berhasil
                    $this->session->set_flashdata('success', 'Tambah data agenda berhasil');
                    // redirect atau berpindah ke halaman tertentu
                    redirect('manager/agenda/index', 'refresh');
                }
            // jika validasi gagal maka perintah yang dijalankan
            } else {
                // memasukkan pesan error kedalam sebuah variabel
                $this->session->set_flashdata('error', validation_errors());
                // redirect atau berpindah kehalaman tertentu
                redirect('manager/agenda/tambah');
            }

        }
        // menampilkan view tertentu untuk halaman tambah agenda
        $this->load->view('manager/agenda/tambah', $data);

    }
// merubah sebuah agenda
    public function ubah($id)
    {
        // beri variable active pada menu bar
        $data['rapat_active'] = 'active';
        // title dari sebuah halaman
        $data['title'] = 'Halaman rubah agenda';
        // menampilkan data satu agenda
        $data['agenda'] = $this->m_agenda->satu_agenda($id);
        // menampilkan semua ruangan yang ada
        $data['ruangan'] = $this->m_ruangan->semua_ruangan();
        $data['agenda_peserta'] = $this->m_agenda->agenda_peserta($id);
        // menampilkan semua profesi yang ada
        $data['profesi'] = $this->m_profesi->user_profesi();
        // validasi input form ubah agenda
        $this->form_validation->set_rules('nama_agenda', 'Nama Agenda', 'required');
        $this->form_validation->set_rules('ruangan', 'Ruangan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jam', 'Jam', 'required');
        $this->form_validation->set_rules('menit', 'Menit', 'required');
// struktur kendali jika input post tidak kosong
        if(!empty($_POST)){
            // struktur kendali jika validasi berhasil atau gagal
            if($this->form_validation->run()=== true){
                // set waktu dari agenda
                $waktu = $this->input->post('tanggal').' '.$this->input->post('jam').':'.$this->input->post('menit').':00';
                // memasukkan data input kedalam sebuah variable data
                $data = [
                    'nama_agenda' =>$this->input->post('nama_agenda'),
                    'ruangan' => $this->input->post('ruangan'),
                    'waktu' => date('Y-m-d H:i:s', strtotime($waktu)) ,
                    'status' => $this->input->post('status'),
                    'keterangan' => $this->input->post('keterangan')
                ];
                // rubah data kedalam database
                $rubah = $this->m_agenda->rubah($id, $data);
                // struktur kendali jika berhasil merubah data
                if($rubah){
                    $this->session->set_flashdata('success', 'Rubah data agenda berhasil');
                    redirect('manager/agenda/index', 'refresh');
                }
// ketika validasi gagal
            } else{
                // memasukkan pesan error kedalam sebuah variable error
                $this->session->set_flashdata('error', validation_errors());
                // redirect atau berpindah kehalman tertentu
                redirect('manager/agenda/ubah/'.$id);
            }
        }
        // menampilkan view untuk halman rubah agenda
        $this->load->view('manager/agenda/ubah', $data);

    }
// fungsi untuk menghapus agenda
    public function hapus($id)
    {
        // set variable active untuk menubar
        $data['rapat_active'] = 'active';
        // perintah hapus data dari database
        $hapus = $this->m_agenda->hapus($id);
        // struktur kendali jika berhasil menghapus atau tidak
        if($hapus){
            $this->session->set_flashdata('success', 'Hapus agenda berhasil');
            redirect('manager/agenda/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Hapus agenda gagal');
            redirect('manager/agenda/index', 'refresh');
        }


    }
    // menampilkan siapa saja peserta dari sebuah agenda
    public function peserta($id_agenda){
        // atur variable active untuk menubar
        $data['rapat_active'] = 'active';
        // title halaman
        $data['title'] = 'Daftar peserta';
        // data peserta yang diundang pada sebuah agenda
        $data['peserta'] = $this->m_agenda->peserta($id_agenda);
        // tampilan untuk halaman list peserta
        $this->load->view('manager/agenda/peserta', $data);
    }
}
