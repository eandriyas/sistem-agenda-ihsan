<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Profile extends Auth
{

    public function __construct()
    {
        parent::__construct();
        $this->is_login_manager();

    }

    public function edit($id)
    {
        $data['title'] = 'Edit your profile';
        $data['user'] = $this->m_user->get_user($id);
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('hp', 'No Handphone', 'required');


        if (!empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $data = [
                    'nama' => $this->input->post('nama'),
                    'email' => $this->input->post('email'),
                    'password' => $this->input->post('password'),
                    'no_hp' => $this->input->post('hp'),
                    'alamat' => $this->input->post('alamat')
                ];
                $update = $this->m_user->update($id, $data);
                if ($update) {
                    $this->session->set_flashdata('success', 'Ubah data Berhasil');
                    redirect('manager/profile/edit/' . $id, 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Ubahd data gagal');
                    redirect('manager/profile/edit/' . $id, 'refresh');
                }
            }else {
                $this->session->set_flashdata('error', validation_errors());
                $data['error'] = (validation_errors()) ? validation_errors() : $this->session->set_flasdata('error');
                redirect('manager/profile/edit/'.$id, 'refresh');
            }

        }
        $this->load->view('manager/profile/ubah', $data);
    }
}
