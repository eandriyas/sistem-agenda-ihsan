<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Ruangan extends Auth
{

    public function __construct()
    {
        parent::__construct();
        $this->is_login_manager();
        $this->load->model('m_ruangan');

    }

    public function index()
    {
        $data['ruangan_active'] = 'active';
        $data['title'] = 'Halaman Ruangan';
        $data['ruangan'] = $this->m_ruangan->semua_ruangan();
        $this->load->view('manager/ruangan/index', $data);
    }

    public function tambah()
    {
        $data['ruangan_active'] = 'active';
        $data['title'] = 'Tambah Ruangan';
        $this->form_validation->set_rules('nama_ruangan', 'Nama Ruangan', 'required');

        if (!empty($_POST)) {
            if ($this->form_validation->run() === true) {
                $data = [
                    'nama_ruangan' => $this->input->post('nama_ruangan'),
                    'keterangan' => $this->input->post('keterangan'),
                    'status' => 'aktif'
                ];
                $tambah = $this->m_ruangan->tambah($data);
                if($tambah){
                    $this->session->set_flashdata('success', 'Tambah ruangan berhasil');
                    redirect('manager/ruangan/index', 'refresh');
                }

            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('manager/ruangan/tambah', 'refresh');
            }

        }
        $this->load->view('manager/ruangan/tambah', $data);

    }

    public function ubah($id)
    {
        $data['title'] = 'Rubah data ruangan';
        $data['ruangan_active'] ='active';
        $data['ruangan'] = $this->m_ruangan->satu_ruangan($id);

        $this->form_validation->set_rules('nama_ruangan', 'Nama Ruangan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if(!empty($_POST)){
            if($this->form_validation->run() === true){
                $data = [
                  'nama_ruangan' => $this->input->post('nama_ruangan'),
                    'status' => $this->input->post('status'),
                    'keterangan' => $this->input->post('keterangan')
                ];
                $rubah = $this->m_ruangan->rubah($id, $data);
                if($rubah){
                    $this->session->set_flashdata('success', 'Rubah data ruangan berhasil');
                    redirect('manager/ruangan/index', 'refresh');
                }

            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('manager/ruangan/ubah/'.$id, 'refresh');
            }
        }

        $this->load->view('manager/ruangan/ubah', $data);

    }
    public function hapus($id){
        $hapus = $this->m_ruangan->hapus($id);
        $this->session->set_flashdata('success', 'Berhasil menghapus ruangan');
        redirect('manager/ruangan/index');
    }


}
