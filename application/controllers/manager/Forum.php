<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Forum extends Auth
{
    public function __construct()
    {
        parent::__construct();
        $this->is_login_manager();

    }

    public function index()
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman Forum';
        $data['forum'] = $this->m_forum->semua_forum();
        $this->load->view('manager/forum/index', $data);
    }

    public function tambah()
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman tambah diskusi';

        if(!empty($_POST)){
           $this->form_validation->set_rules('judul', 'Judul', 'required');
           $this->form_validation->set_rules('isi', 'Isi', 'required');

           if($this->form_validation->run() === true){
                $data = [
                'judul' => $this->input->post('judul'),
                'isi' => $this->input->post('isi'),
                'created_by' => $this->session->userdata('manager')['user_id'] ,
                'created_at' => date('Y-m-d H:i:s')
                ];

                $tambah = $this->m_forum->tambah($data);

                if($tambah){
                    $this->session->set_flashdata('success', 'Berhasil menambahkan Diskusi');
                    redirect('manager/forum');
                }

           } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('manager/forum/tambah');
           }
        }

        $this->load->view('manager/forum/tambah', $data);

    }

    public function ubah($id)
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman rubah diskusi';
        $data['forum'] = $this->m_forum->satu_forum($id);
        $data['komentar'] = $this->m_komentar->semua_komentar($id);

        if(!empty($_POST)){
            $this->form_validation->set_rules('judul', 'Judul DIskusi', 'required');
            $this->form_validation->set_rules('isi', 'Isi diskusi', 'required');

            if($this->form_validation->run() === true){
                $data = [
                    'judul' => $this->input->post('judul'),
                    'isi' => $this->input->post('isi')
                ];
                $ubah = $this->m_forum->ubah($id, $data);
                if($ubah){
                    $this->session->set_flashdata('success', 'Berhasil ubah data diskusi');
                    redirect('manager/forum/index');
                }
            }
        }


        $this->load->view('manager/forum/ubah', $data);

    }
    public function ubahKomentar(){

        $ubah = $this->m_komentar->ubah($this->input->post('id_komentar'), ['isi' => $this->input->post('isi')]);
        if($ubah){
            $data = ['message' => 'berhasil'];
            echo json_encode($data);
        }
    }
    public function hapusKomentar($id){
        $hapus = $this->m_komentar->hapus($id);
        if($hapus){
            $data = ['message' => 'berhasil'];
            echo json_encode($data);
        }
    }

    public function detail($id)
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman detail diskusi';
        $data['forum'] = $this->m_forum->satu_forum($id);
        $data['komentar'] = $this->m_komentar->semua_komentar($id);
        $this->load->view('manager/forum/detail', $data);


    }
    public function tambah_komentar(){
        $this->form_validation->set_rules('isi', 'Isi komentar', 'required');
        $id_forum = $this->input->post('id_forum');
        $isi = $this->input->post('isi');
        if($this->form_validation->run() === true){
            $data = [
                'id_forum' => $id_forum,
                'isi' => $isi,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('manager')['user_id']
                ];
                $tambah = $this->m_komentar->tambah($data);
                if($tambah){
                    $this->session->set_flashdata('success', 'Berhasil menambahkan komentar');
                    redirect('manager/forum/detail/'.$id_forum);
                }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('manager/forum/detail/'.$id_forum);
        }
    }
    public function hapus($id)
    {
        $hapus = $this->m_forum->hapus($id);

        if($hapus){
            $this->session->set_flashdata('success', 'Berhasil menghapus diskusi.');
            redirect('manager/forum/index');
        }



    }
}
