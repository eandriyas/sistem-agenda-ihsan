<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Profesi extends Auth
{

    public function __construct()
    {
        parent::__construct();
        $this->is_login_manager();


    }
    public function index()
    {
        $data['profesi_active'] = 'active';
        $data['title'] = 'Halaman Profesi';
        $data['profesi'] = $this->m_profesi->semua_profesi();
        $this->load->view('manager/profesi/index', $data);
    }

    public function tambah()
    {
        $data['profesi_active'] = 'active';
        $data['title'] = 'Halaman tambah profesi';
        $this->form_validation->set_rules('nama_profesi', 'Nama profesi', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

        if(!empty($_POST)){

            if($this->form_validation->run() === true){

                $data = [
                    'nama_profesi' => $this->input->post('nama_profesi'),
                    'keterangan' => $this->input->post('keterangan')
                ];
                $tambah = $this->m_profesi->tambah($data);
                if($tambah){
                    $this->session->set_flashdata('success', 'Tambah data profesi berhasil');
                    redirect('manager/profesi/index', 'refresh');
                }

            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('manager/profesi/tambah');
            }

        }
        $this->load->view('manager/profesi/tambah', $data);

    }

    public function ubah($id)
    {
        $data['profesi_active'] = 'active';
        $data['title'] = 'Halaman rubah profesi';
        $data['profesi'] = $this->m_profesi->satu_profesi($id);

        $this->form_validation->set_rules('nama_profesi', 'Nama Profesi', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

        if(!empty($_POST)){
            if($this->form_validation->run()=== true){

                $data = [
                  'nama_profesi' => $this->input->post('nama_profesi'),
                    'keterangan' => $this->input->post('keterangan')
                ];
                $rubah = $this->m_profesi->rubah($id, $data);
                if($rubah){
                    $this->session->set_flashdata('success', 'Rubah data profesi berhasil');
                    redirect('manager/profesi/index', 'refresh');
                }

            } else{
                $this->session->set_flashdata('error', validation_errors());
                redirect('manager/profesi/ubah/'.$id);
            }
        }
        $this->load->view('manager/profesi/ubah', $data);

    }

    public function hapus($id)
    {
        $data['profesi_active'] = 'active';
        $hapus = $this->m_profesi->hapus($id);
        if($hapus){
            $this->session->set_flashdata('success', 'Hapus profesi berhasil');
            redirect('manager/profesi/index', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Hapus profesi gagal');
            redirect('manager/profesi/index', 'refresh');
        }

    }
}
