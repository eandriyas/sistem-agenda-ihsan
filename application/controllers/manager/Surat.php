<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Surat extends Auth {
	public function __construct()
	{
		parent::__construct();
		$this->is_login_manager();

	}

	public function index(){
		$data['surat_active'] = 'active';
		$data['title'] = 'Halaman Surat';
		$data['surat'] = $this->m_surat->semua_surat();
		$this->load->view('manager/surat/index', $data);
	}

	public function tambah()
	{
		$data['surat_active'] = 'active';
		$data['title'] = 'Halaman tambah rapat';
		$this->load->view('manager/surat/tambah', $data);

	}
	public function ubah($id_surat){
		$data['surat_active'] = 'active';
		$data['title'] = 'Halaman Rubah surat';
		$data['surat'] = $this->m_surat->satu_surat($id_surat);
		// validasi form
		$this->form_validation->set_rules('tujuan_dinas', 'Tujuan dinas', 'required');
		$this->form_validation->set_rules('biaya_transportasi', 'Biaya Transportasi', 'required');
		$this->form_validation->set_rules('biaya_penginapan', 'Biaya penginapan', 'required');
		$this->form_validation->set_rules('biaya_konsumsi', 'Biaya Konsumsi', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('keterangan', 'Keternangan', 'required');

		if (!empty($_POST)) {
			if($this->form_validation->run() === true){
				$data = [
					'tujuan_dinas' => $this->input->post('tujuan_dinas'),
					'biaya_transportasi' => $this->input->post('biaya_transportasi'),
					'biaya_penginapan' => $this->input->post('biaya_penginapan'),
					'biaya_konsumsi' => $this->input->post('biaya_konsumsi'),
					'tanggal' => $this->input->post('tanggal'),
					'keterangan' => $this->input->post('keterangan'),
					'status' => $this->input->post('status')
				];
				$rubah = $this->m_surat->update($id_surat, $data);
				if($rubah){
					$this->session->set_flashdata('success', 'Berhasil merubah surat');
					redirect('manager/surat/index', 'refresh');
				} else {
					$this->session->set_flashdata('error', 'Gagal merubah data surat');
					redirect('manager/surat/index', 'refresh');
				}
			} else {
				$this->session->set_flashdata('error', validation_error());
				redirect('manager/surat', 'refresh');
			}
		}

		$this->load->view('manager/surat/ubah', $data);
	}
	public function hapus($id){
		$hapus = $this->m_surat->hapus($id);
		if($hapus){
			$this->session->set_flashdata('success', 'Berhasil menghapus surat');
			redirect('manager/surat', 'refresh');
			
		}

	}
}
