<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Home extends Auth
{
    public function __construct()
    {
        parent::__construct();
        $this->is_login_manager();


    }

    public function index()
    {


        $data['home_active'] = 'active';
        $data['title'] = 'Halaman Home';
        $this->load->view('manager/home', $data);
    }


}
