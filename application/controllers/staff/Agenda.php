<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Agenda extends Auth
{

    public function __construct()
    {
        parent::__construct();
        $this->is_login_staff();
        $this->load->library('staff');
       


    }
    public function index()
    {
        $data['rapat_active'] = 'active';
        $data['title'] = 'Halaman Rapat';
        $sess = $this->session->userdata('staff');
        $data['count'] = $this->m_agenda->count_status_kehadiran($sess['user_id']); 
        $data['agenda'] = $this->m_agenda->semua_agenda_peserta($sess['user_id']);

        $this->load->view('staff/agenda/index', $data);
    }

   

    public function ubah($id_peserta, $id_agenda, $status)
    {
        if($status == 'hadir'){
            $a = 1;
        } else {
            $a = 2;
        }

        $data = [
            'status_kehadiran' => $a
        ];
        // print_r($data); die();
        $ubah = $this->m_agenda->ubah_peserta($id_peserta, $id_agenda, $data);
        if($ubah){
            $this->session->set_flashdata('success', 'Berhasil merubah agenda');
            redirect('staff/agenda', 'refresh');
        } else {
            $this->session->set_flashdata('error','Gagal merubah agenda');
            redirect('staff/agenda', 'refresh');
        }
        
       

    }

    public function hapus()
    {
        $data['rapat_active'] = 'active';


    }
}
