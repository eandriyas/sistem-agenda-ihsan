<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Forum extends Auth
{
    public function __construct()
    {
        parent::__construct();
        $this->is_login_staff();

    }

    public function index()
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman Forum';
        $data['forum'] = $this->m_forum->semua_forum();
        $data['count'] = $this->m_agenda->count_status_kehadiran($this->session->userdata('staff')['user_id']); 
        $this->load->view('staff/forum/index', $data);
    }

    public function tambah()
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman tambah diskusi';
        $data['count'] = $this->m_agenda->count_status_kehadiran($this->session->userdata('staff')['user_id']); 
       
        if(!empty($_POST)){
           $this->form_validation->set_rules('judul', 'Judul', 'required');
           $this->form_validation->set_rules('isi', 'Isi', 'required');

           if($this->form_validation->run() === true){
                $data = [
                'judul' => $this->input->post('judul'),
                'isi' => $this->input->post('isi'),
                'created_by' => $this->session->userdata('staff')['user_id'] ,
                'created_at' => date('Y-m-d H:i:s')
                ];

                $tambah = $this->m_forum->tambah($data);

                if($tambah){
                    $this->session->set_flashdata('success', 'Berhasil menambahkan Diskusi');
                    redirect('staff/forum');
                }

           } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('staff/forum/tambah');
           }
        }

        $this->load->view('staff/forum/tambah', $data);

    }

    public function ubah()
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman rubah diskusi';
        $this->load->view('staff/rapat/ubah', $data);

    }

    public function detail($id)
    {
        $data['forum_active'] = 'active';
        $data['title'] = 'Halaman detail diskusi';
        $data['forum'] = $this->m_forum->satu_forum($id);
        $data['komentar'] = $this->m_komentar->semua_komentar($id);
        $data['count'] = $this->m_agenda->count_status_kehadiran($this->session->userdata('staff')['user_id']); 
        $this->load->view('staff/forum/detail', $data);


    }
    public function hapus()
    {
        $data['forum_active'] = 'active';


    }
     public function tambah_komentar(){
        $this->form_validation->set_rules('isi', 'Isi komentar', 'required');
        $id_forum = $this->input->post('id_forum');
        $isi = $this->input->post('isi');
        if($this->form_validation->run() === true){
            $data = [
                'id_forum' => $id_forum,
                'isi' => $isi,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('staff')['user_id']
                ];
                $tambah = $this->m_komentar->tambah($data);
                if($tambah){
                    $this->session->set_flashdata('success', 'Berhasil menambahkan komentar');
                    redirect('staff/forum/detail/'.$id_forum);
                }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('staff/forum/detail/'.$id_forum);
        }
    }
}
