<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Surat extends Auth {
	public function __construct()
	{
		parent::__construct();
		$this->is_login_staff();

	}

	public function index(){
		$sess = $this->session->userdata('staff');
        $data['count'] = $this->m_agenda->count_status_kehadiran($sess['user_id']);
		$data['surat_active'] = 'active';
		$data['title'] = 'Halaman Surat';
		$data['surat'] = $this->m_surat->semua_surat();
		$this->load->view('staff/surat/index', $data);
	}

	public function tambah()
	{
		$sess = $this->session->userdata('staff');
        $data['count'] = $this->m_agenda->count_status_kehadiran($sess['user_id']);
		$data['surat_active'] = 'active';
		$data['title'] = 'Halaman tambah surat';

		// valdasi input
		$this->form_validation->set_rules('tujuan_dinas', 'Tujuan dinas', 'required');
		$this->form_validation->set_rules('biaya_transportasi', 'Biaya Transportasi', 'required');
		$this->form_validation->set_rules('biaya_penginapan', 'Biaya penginapan', 'required');
		$this->form_validation->set_rules('biaya_konsumsi', 'Biaya Konsumsi', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('keterangan', 'Keternangan', 'required');
		if(!empty($_POST)){
			if($this->form_validation->run() === true){
				$data = [
					'tujuan_dinas' => $this->input->post('tujuan_dinas'),
					'biaya_transportasi' => $this->input->post('biaya_transportasi'),
					'biaya_penginapan' => $this->input->post('biaya_penginapan'),
					'biaya_konsumsi' => $this->input->post('biaya_konsumsi'),
					'tanggal' => $this->input->post('tanggal'),
					'keterangan' => $this->input->post('keterangan'),
					'status' => 2
				];
				$tambah = $this->m_surat->tambah($data);
				if($tambah){
					$this->session->set_flashdata('success', 'Berhasil menambahkan surat');
					redirect('staff/surat/index', 'refresh');
				} else {
					$this->session->set_flashdata('error', 'Gagal menambahkan data');
					redirect('staff/surat/index', 'refresh');
				}
			} else {
				$this->session->set_flashdata('error', validation_errors());
				redirect('staff/surat/tambah', 'refresh');
			}
		}
		$this->load->view('staff/surat/tambah', $data);

	}
	public function ubah($id){
		$sess = $this->session->userdata('staff');
        $data['count'] = $this->m_agenda->count_status_kehadiran($sess['user_id']);
		$data['surat_active'] = 'active';
		$data['title'] = 'Halaman Rubah surat';
		$data['surat'] = $this->m_surat->satu_surat($id);
		// validasi form
		$this->form_validation->set_rules('tujuan_dinas', 'Tujuan dinas', 'required');
		$this->form_validation->set_rules('biaya_transportasi', 'Biaya Transportasi', 'required');
		$this->form_validation->set_rules('biaya_penginapan', 'Biaya penginapan', 'required');
		$this->form_validation->set_rules('biaya_konsumsi', 'Biaya Konsumsi', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('keterangan', 'Keternangan', 'required');
		
		if (!empty($_POST)) {
			if($this->form_validation->run() === true){
				$data = [
					'tujuan_dinas' => $this->input->post('tujuan_dinas'),
					'biaya_transportasi' => $this->input->post('biaya_transportasi'),
					'biaya_penginapan' => $this->input->post('biaya_penginapan'),
					'biaya_konsumsi' => $this->input->post('biaya_konsumsi'),
					'tanggal' => $this->input->post('tanggal'),
					'keterangan' => $this->input->post('keterangan'),
					'status' => 2
				];
				$rubah = $this->m_surat->update($id, $data);
				if($rubah){
					$this->session->set_flashdata('success', 'Berhasil merubah surat');
					redirect('staff/surat/index', 'refresh');
				} else {
					$this->session->set_flashdata('error', 'Gagal merubah data surat');
					redirect('staff/surat/index', 'refresh');
				}
			} else {
				$this->session->set_flashdata('error', validation_error());
				redirect('staff/surat', 'refresh');
			}
		}

		$this->load->view('staff/surat/ubah', $data);
	}
	public function hapus(){

	}
}
