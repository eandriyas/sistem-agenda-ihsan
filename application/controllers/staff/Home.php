<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('application/controllers/Auth.php');

class Home extends Auth
{
    public function __construct()
    {
        parent::__construct();
        $this->is_login_staff();
        


    }

    public function index()
    {

         $sess = $this->session->userdata('staff');
        $data['count'] = $this->m_agenda->count_status_kehadiran($sess['user_id']); 
        $data['home_active'] = 'active';
        $data['title'] = 'Halaman Home Staff';
        $this->load->view('staff/home', $data);
    }


}
