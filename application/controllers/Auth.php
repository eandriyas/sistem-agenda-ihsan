<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// fungsi untuk authentikasi user 
class Auth extends CI_Controller
{
    // fungsi yang akan dijalankan disemua fungsi turunan
    public function __construct()
    {
        parent::__construct();
        // set timezone
        date_default_timezone_set("Asia/Jakarta");
        // load model
        $this->load->model('m_user');
        $this->load->model('m_ruangan');
        $this->load->model('m_agenda');
        $this->load->model('m_profesi');
        $this->load->model('m_surat');
        $this->load->model('m_forum');
        $this->load->model('m_komentar');

    }
// cek untuk login khusus manager
    public function is_login_manager()
    {

        if ($this->session->userdata('manager') == '') {
            $this->session->set_flashdata('error', 'Kamu tidak boleh mengakses halaman ini, silahkan login terlebih dahulu');

            redirect('auth/login', 'refresh');

        }

    }
// cek unuk login khusus staff/karyawan
    public function is_login_staff()
    {
        if ($this->session->userdata('staff') == '') {
            $this->session->set_flashdata('error', 'Kamu tidak boleh mengakses halaman ini, silahkan login terlebih dahulu');

            redirect('auth/login', 'refresh');

        }

    }
// menampilkan halaman login baik untuk staff maupun karyawan
    public function login()
    {
        $data['home_active'] = 'active';
        $data['title'] = 'Halaman Login';
        $this->load->view('login', $data);
    }
// login proses
    public function login_proses()
    {
// validasi untuk input form login
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
// struktur kendali jika validasi berhasil atau gagal
        if ($this->form_validation->run() == true) {
            // memasukkan hasil input dari form kedalam sebuah variabel
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            // cek didatabase apakah user dan password match atau tidak
            $cek_login = $this->m_user->cek_login($username, $password);
            // struktur kendali jika hasil cek didatabase tidak kosong
            if (!empty($cek_login)) {
                // jika jabatan user = manager maka diarahkan kehalaman khusus manager
                if ($cek_login->jabatan == 'manager') {
                    // atur data session
                    $data = [
                        'user_id' => $cek_login->id,
                        'username' => $cek_login->username
                    ];
                    $data_update = ['last_login' => date('Y-m-d H:i:s')];
                    $this->m_user->update($cek_login->id, $data_update);
                    // set pesan sukses login
                    $this->session->set_userdata('manager', $data);
                    $this->session->set_flashdata('success', 'Anda berhasil login');
                    // redirect atau berpindah kehalaman tertentu
                    redirect('manager/home/index', 'refresh');
// set sebagai staff jika jabatan selain manager
                } else {
                    // set data session
                    $data = [
                        'user_id' => $cek_login->id,
                        'username' => $cek_login->username
                    ];

                    $data_update = ['last_login' => date('Y-m-d H:i:s')];
                    $this->m_user->update($cek_login->id, $data_update);

                    $this->session->set_userdata('staff', $data);
                    // set pesan sukses login
                    $this->session->set_flashdata('success', 'Selamat, anda berhasil login sebagai staff');
                    // redirect atau berpindah kehalaman tertentu
                    redirect('staff/home/index', 'refresh');

                }
                // jika data user tidak ditemukan didalam database
            } else {
                $this->session->set_flashdata('error', 'Username/password salah!');
                redirect('auth/login', 'refresh');

            }
            // jika validasi error

        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('auth/login', 'refresh');

        }


    }
    // fungsi untuk mendaftar kedalam sistem

    public function daftar()
    {
        // title halaman
        $data['title'] = 'Halaman Pendaftaran';
        // data semua profesi yang ada
        $data['profesi'] = $this->m_profesi->semua_profesi();
        // validasi form input
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('hp', 'No Handphone', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        // struktur kendali jika input post tidak kosong
        if(!empty($_POST)){
            // struktur kendali jika validasi form berhasil atau tidak
            if($this->form_validation->run() ===true){
                // data yang akan dimasukkan kedalam database
                $data = [
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                    'nama' => $this->input->post('nama'),
                    'alamat' => $this->input->post('alamat'),
                    'email' => $this->input->post('email'),
                    'no_hp' => $this->input->post('hp'),
                    'jabatan' => 'staff'
                ];
                // tambah data kedalam database
                $tambah = $this->m_user->tambah($data);
                // id dari query yang baru saja ditambahkan kedalam database
                $id = $this->db->insert_id();
                // user profesi
                $data_profesi = [
                    'id_profesi' => $this->input->post('profesi'),
                    'id_user' => $id
                ];
                $this->m_profesi->user_tambah($data_profesi);
                // struktur kendali jika berhasil menambah data kedalam database
                if($tambah){
                    // set pesan sukses login kedalam sistem
                    $this->session->set_flashdata('success', 'Anda berhasil mendaftar');
                    $data_session = [
                        'user_id' => $id,
                        'username' =>$this->input->post('username')
                    ];
                    $this->session->set_userdata('staff', $data_session);
                    redirect('staff/home/index', 'refresh');
                }

            }else {
                // pesan jika validasi form gagal
                $this->session->set_flashdata('error', validation_errors());
                redirect('auth/daftar');
            }
        }
        // view untuk menampilkan halaman daftar
        $this->load->view('daftar', $data);

    }
// fungsi untuk melakukan logout
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth/login');

    }
}
